from django.contrib import admin
from store.models import Book, Category

# Register your models here.
admin.site.register(Category)
admin.site.register(Book)
