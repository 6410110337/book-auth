from django.db import models

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=250, null=False, blank=False)

    def __str__(self):
        return self.title


class Book(models.Model):
    title = models.CharField(max_length=250, null=False, blank=False)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
