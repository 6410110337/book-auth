from .models import Book
from store.serializers import BookSerializer
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response


class BookModelViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


@api_view()
def whoami(request):
    result = {"username": request.user.username}
    return Response(result)
